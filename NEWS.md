# commonCodes 0.1.0

* Added a `NEWS.md` file to track changes to the package.

* Added support for Global Administrative Boundaries (GADM)

* Added support for `raster` package's country codes dataset

* Added support for United Nations High Commissioner for Refugees Populations of Concern Time Series

* Added support for United Nations Migrant Stock Populations (2017 Revision)

* Added support for Mass Episodes of Political Violence

* Added support for International Organization for Standardization

* Added support for The Correlates of War Project

* Added support for Gleditsch and Ward country codes

* Added support for International Monetary Fund country codes
