
# commonCodes <img src='man/figures/logo.png' align="right" height="139" />

<!-- badges: start -->

<!-- badges: end -->

Country codes. The bane of geographers, political scientists, and a
variety of research fields. Despite a handful of tools developed to
address the issue of matching and harmonizing country codes between
disparate data sets, there still remains several gaps and
inefficiencies. Although the widely used R package
[`countrycode`](https://cran.r-project.org/web/packages/countrycode/index.html)
provides fantastic tools for converting between codes, it often
struggles with several climate-security datasets. These can be the
result of odd nomenclature, typos, missing values, or a slew of other
unforseen issues that arise when attempting to harmonize nation-state
data. In addition to source data errors, `countrycode()` may generate
unnoticed one-to-many or many-to-one conversions and fail to produce
values for disputed or nation-states not recognized by international
organizations.

`commonCodes` supplements the `countrycode` package by providing a
multitude of “hardcoded” fixes for typos, missing data, and other errors
pervasive in a variety of climate security datasets. The end result is a
singular `data frame` / `data.table` with a harmonized listing of
countries from several datasets, widely used coding schemes, and their
membership status for multiple international organizations. This package
and included dataset works best in conjunction with additional DANTE
Project R packages like
[`untools`](https://gitlab.com/dante-sttr/untools) and
[`conflictr`](https://gitlab.com/dante-sttr/conflictr), however, the
dataset provides added value for any researcher in the human geography
and political science sectors.

# Dataset Support

The `commonCodes` dataset includes entries for 260 nation-states and
territories, their written name as presented in the associated dataset,
their official code in several widely used schemes, and membership
status in multiple international organizations. To see detailed
information for preparation of the `commonCodes` dataset please review
the
[commonCodes.R](https://gitlab.com/dante-sttr/commoncodes/tree/master/data-raw)
script. `commonCodes` currently provides support for the following
datasets and coding schemes:

**Data Sets:**

  - Global Administrative Boundaries (GADM)
  - The `raster` package’s country codes dataset
  - United Nations High Commissioner for Refugees Populations of Concern
      - Refugee Time Series
      - Asylum Applications
      - Asylum Applications Decisions
      - Internal Displacement Monitoring Center’s IDPs
  - United Nations Migrant Stock Populations (2019 Revision)
  - United Nations Migrant Stock Populations (2017 Revision)
  - Mass Episodes of Political Violence (MEPV)
  - International Monetary Direction of Trade Statistics (Annual)
  - International Monetary Direction of Trade Statistics (Monthly)

**Coding Schemes:**

  - International Organization for Standardization
  - The Correlates of War Project
  - Gleditsch and Ward country codes

**Internation Memberships:**

  - Organization for Economic Co-operation and Development (OECD)
  - European Union (EU)

# Installation

You can install the released version of commonCodes from
[GitLab](https://gitlab.com/) with:

``` r
devtools::install_gitlab("dante-sttr/commoncodes", dependencies=TRUE)
```

# Installation With Windows

To install R packages over Git on a Windows system, you must install
Rtools first. The latest version of Rtools is available
[here](https://cran.r-project.org/bin/windows/Rtools/). Furthermore, you
may experience difficulty installing R packages over Git if you utilize
a Windows machine on a network with Active Directory or shared network
drives. To enable proper package installation under these circumstances
please follow [this
guide](https://dante-sttr.gitlab.io/dante-vignettes/windows-pkg-inst/windows-pkg-inst.html).

# Example

The commonCodes dataset is available by calling `commonCodes`. More
information on the dataset structure is available with `help()`.

``` r
library(commonCodes)

help(commonCodes)
```

    ## starting httpd help server ... done

You can call on a specific dataset with the following code:

``` r
mepv.codes<-commonCodes::commonCodes[,.(mepv, iso3)]
mepv.codes[1:10]
```

    ##            mepv iso3
    ##  1:        <NA> <NA>
    ##  2:        <NA>  ABK
    ##  3:        <NA>  ABW
    ##  4: Afghanistan  AFG
    ##  5:      Angola  AGO
    ##  6:        <NA>  AIA
    ##  7:        <NA>  ALA
    ##  8:     Albania  ALB
    ##  9:        <NA>  AND
    ## 10:        <NA>  ANT

`NA` entries indicate the nation or territory is not present in the
specified dataset. In order to retrieve a full list of only nations or
territories contained in the specified dataset, wrap the row indexing in
a not NA statement:

``` r
mepv.codes<-commonCodes::commonCodes[!(is.na(mepv)),.(mepv, iso3)]
mepv.codes
```

    ##                      mepv iso3
    ##   1:          Afghanistan  AFG
    ##   2:               Angola  AGO
    ##   3:              Albania  ALB
    ##   4: United Arab Emirates  ARE
    ##   5:            Argentina  ARG
    ##  ---                          
    ## 164:               Kosovo  XKO
    ## 165:                Yemen  YEM
    ## 166:         South Africa  ZAF
    ## 167:               Zambia  ZMB
    ## 168:             Zimbabwe  ZWE

Lastly, `commonCodes` now supports indexing for OECD and EU memberships.
To quickly create a vector of OECD members and their listed names in the
GADM country boundary dataset along with their ISO3 codes, use the
following call:

``` r
oecd<-unique(commonCodes::commonCodes[oecd=="1",.(gadm,iso3)])
oecd
```

    ##               gadm iso3
    ##  1:      Australia  AUS
    ##  2:        Austria  AUT
    ##  3:        Belgium  BEL
    ##  4:         Canada  CAN
    ##  5:    Switzerland  CHE
    ##  6:          Chile  CHL
    ##  7: Czech Republic  CZE
    ##  8:        Germany  DEU
    ##  9:        Denmark  DNK
    ## 10:          Spain  ESP
    ## 11:        Estonia  EST
    ## 12:        Finland  FIN
    ## 13:         France  FRA
    ## 14: United Kingdom  GBR
    ## 15:         Greece  GRC
    ## 16:        Hungary  HUN
    ## 17:        Ireland  IRL
    ## 18:        Iceland  ISL
    ## 19:         Israel  ISR
    ## 20:          Italy  ITA
    ## 21:          Japan  JPN
    ## 22:    South Korea  KOR
    ## 23:      Lithuania  LTU
    ## 24:     Luxembourg  LUX
    ## 25:         Latvia  LVA
    ## 26:         Mexico  MEX
    ## 27:    Netherlands  NLD
    ## 28:         Norway  NOR
    ## 29:    New Zealand  NZL
    ## 30:         Poland  POL
    ## 31:       Portugal  PRT
    ## 32:       Slovakia  SVK
    ## 33:       Slovenia  SVN
    ## 34:         Sweden  SWE
    ## 35:         Turkey  TUR
    ## 36:  United States  USA
    ##               gadm iso3
