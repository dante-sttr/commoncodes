#' Display Index of Character Strings With Non-ASCII Characters
#'
#' This function displays logical binary displaying indexed location of
#' character strings in a character vector that contains non-ASCII character.
#' This is an important check to run on new datasets integrated into
#' \code{commonCodes}, because the package will not build and cause many
#' downstream problems.
#'
#' @param x An input character string or character vector. Typically this would
#'   be a column of country names or other identifiers.
#'
#' @return A vector of logical values \code{TRUE, FALSE} indicating the presence
#'   of non-ASCII characters.
#' @export
nonAscii<-function(x){
  grepl("[^ -~]", x)
}
