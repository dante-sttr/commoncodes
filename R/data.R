#' Unique GADM Countries
#'
#' A dataset containing 256 unique nation-state or territory names
#' and ISO3 character codes from the GADM spatial dataset.
#'
#' @format A data frame / data.table with 256 rows and 2 variables:
#' \describe{
#'   \item{iso3}{ISO3 character code}
#'   \item{gadm.name}{Nation-state or territory name.}
#'   ...
#' }
#' @source \url{https://gadm.org/download_world.html}
#'
"gadmsub"

#' DANTE Project commonCodes Master List
#'
#' A \code{data.table, data.frame} cross-referencing nation-state and territory
#' names across several high useage data sets and coding schemes.
#'
#' @format A data frame / data.table with 260 rows and 16 variables:
#' \describe{
#'   \item{iso3}{International Organization for Standardization character 3
#'   digit code (ISO3c).}
#'   \item{unhcr.id}{United Nations High Commissioner on Refugees numeric
#'   country code (July 2020).}
#'   \item{unhcr}{United Nations High Commissioner on Refugees country name
#'   (July 2020).}
#'   \item{unstock19}{Nation-state or territory name listed in the United
#'   Nations Migrant Stock (2019 Revision) dataset.}
#'   \item{unstock17}{Nation-state   or territory name listed in the United
#'   Nations Migrant Stock (2017 Revision) dataset.}
#'   \item{raster}{Country name and code listed in the R \code{raster} package
#'   (July 2020).}
#'   \item{gadm}{Country name and code listed in the the GADM 3.6 dataset.}
#'   \item{mepvn}{Country or territory numeric code listed in the Mass Episodes
#'   of Political Violence (MEPV) dataset (July 25, 2019).}
#'   \item{mepvc}{Country or territory characer code listed in the Mass Episodes
#'   of Political Violence (MEPV) dataset (July 25, 2019).}
#'   \item{mepv}{Country or territory name listed in the Mass Episodes of
#'   Political Violence (MEPV) dataset (July 25, 2019).}
#'   \item{imf.dot}{Country name listed in the annual and monthly International
#'   Monetary Fund's Direction of Trade Statistics database.}
#'   \item{imf.dotn}{numeric country code listed in the annual and monthly
#'   International Monetary Fund's Direction of Trade Statistics database.}
#'   \item{gwn}{Gleditsch and Ward 3 digit numeric country code.}
#'   \item{gwc}{Gleditsch and Ward 3 digit character country code.}
#'   \item{gw}{Gleditsch and Ward country name.}
#'   \item{cown}{Correlates of War 3 digit numeric country code.}
#'   \item{cowc}{Correlates of War 3 digit character country code.}
#'   \item{cow}{Correlates of War country name.}
#'   \item{oecd}{Binary flag indicating membership to Organisation for Economic
#'   Co-operation and Development (OECD).}
#'   \item{eu}{Binary flag indicating membership to European Union (EU).}
#'   }
"commonCodes"

#' OECD Member Nations
#'
#' A dataset containing current (2019) members of  Organisation for Economic
#' Co-operation and Development (OECD)
#'
#' @format A data frame / data.table with 36 rows and 2 variables:
#' \describe{
#'   \item{oecd.name}{Name of member nation}
#'   \item{oecd.iso3}{ISO3 character code for member nation}
#'   ...}
#' @source \url{http://www.oecd.org/about/members-and-partners/}
#'
"oecd"

#' EU Member Nations
#'
#' A dataset containing current (2019) members of  European Union (EU)
#'
#' @format A data frame / data.table with 28 rows and 2 variables:
#' \describe{
#'   \item{eu.name}{Name of member nation}
#'   \item{eu.iso3}{ISO3 character code for member nation}
#'   ...}
#' @source \url{https://europa.eu/european-union/about-eu/countries_en#tab-0-1}
#'
"eu"
